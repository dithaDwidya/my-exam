import { MyExamPage } from './app.po';

describe('my-exam App', function() {
  let page: MyExamPage;

  beforeEach(() => {
    page = new MyExamPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
