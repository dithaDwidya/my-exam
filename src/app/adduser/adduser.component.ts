import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-adduser',
  templateUrl: './adduser.component.html',
  styleUrls: ['./adduser.component.css']
})
export class AdduserComponent implements OnInit {

  constructor(private api:ApiService) { }

  ngOnInit() {
  }

  bname:string = "";
  bemail:string = "";
  baddress:string= "";
  bphone:string="";
  bcompany:string= "";

  addUser(){
    this.api.user.push({
      "name":this.bname, 
      "email":this.bemail, 
      "address":this.baddress, 
      "phone":this.bphone, 
      "company":this.bcompany})

    this.bname = "";
    this.bemail ="";
    this.baddress="";
    this.bphone="";
    this.bcompany="";
  }
  
}
