import { Injectable } from '@angular/core';

import { Http, Headers, RequestOptions } from '@angular/http';  //Post
import { Observable } from 'rxjs/Rx';

import 'rxjs/add/operator/map'; //map
import 'rxjs/add/operator/catch'; //catch

@Injectable()
export class ApiService {

  constructor(private http:Http) { }

  user:Object[  ] = [
   { name: "",
    email: "",
    address: "",
    phone: "",
    company: ""}
   ];
  
  // bname:string = "";
  // bemail:string = "";
  // baddress:string= "";
  // bphone:string="";
  // bcompany:string= "";

  getData(){
    
    return this.http
    .get('https://jsonplaceholder.typicode.com/users')

    .map(result => result.json()) //map untuk mengconvert json ke object
    .catch(error => Observable.throw(error.json().error) || "Server Error");  //catch untuk menghendle error
  }

 

  addData(obj: Object){

    // let dengan var sama saja cuma let cakupannya lebih kecil
    let body = JSON.stringify(obj); //convert object ke json
    let headers = new Headers({ "Content-Type" : "application/json" });
    let options = new RequestOptions({ headers : headers });

    this.http.post('https://jsonplaceholder.typicode.com/users', body, options)
    .subscribe(result => console.log(result.json()));

  }

}
