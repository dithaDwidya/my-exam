import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';

const userList: Object[] = [
  
]
@Component({
  selector: 'app-userlist',
  templateUrl: './userlist.component.html',
  styleUrls: ['./userlist.component.css']
})
export class UserlistComponent implements OnInit {

  userList: Object[];
 
  constructor(private api:ApiService) { }

  ngOnInit() {
    this.api.getData()
    .subscribe(result => this.userList = result);
  }

  deleteUser(index){
    this.userList.splice(index,1);
  }
}
