import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-edituser',
  templateUrl: './edituser.component.html',
  styleUrls: ['./edituser.component.css']
})
export class EdituserComponent implements OnInit {

  bname:string = "";
  bemail:string = "";
  baddress:string= "";
  bphone:string="";
  bcompany:string= "";

  constructor(private api:ApiService) { }

  a = 0;
  ngOnInit() {
    for (var i = 0; i < this.api.user.length; i++) {
      if(this.api.user[i]["edit"]==1){
        this.bname = this.api.user[i]["name"];
        this.bemail = this.api.user[i]["email"];
        this.baddress= this.api.user[i]["address"];
        this.bphone= this.api.user[i]["phone"];
        this.bcompany= this.api.user[i]["company"];
        this.api.user[i]["edit"] = 0;
        break;
      }
    }
    this.a=i;
  }

  saveUser(){
    this.api.user[this.a]["save"] = 1;
    // for (var i = 0; i < this.databook.books.length; i++) {
      if(this.api.user[this.a]["save"]==1){
        this.api.user[this.a]["name"] = this.bname;
        this.api.user[this.a]["email"] = this.bemail;
        this.api.user[this.a]["address"] = this.baddress;
        this.api.user[this.a]["phone"] = this.bphone;
        this.api.user[this.a]["company"] = this.bcompany;
        this.api.user[this.a]["save"] = 0;
        // break;
      }

    

    alert("Book Saved Succesfully");
  }

}
